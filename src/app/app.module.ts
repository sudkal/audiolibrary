import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MusicListComponent } from './components/music-list/music-list.component';
import { MediaPlayerComponent } from './components/media-player/media-player.component';
import { AudioComponent } from './components/audio/audio.component';
import { MusicdataService } from './services/musicdata.service';
// import { PdfViewerModule } from 'ng2-pdf-viewer'; // <-- Import PdfJsViewerModule module
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FilterPipe } from './pipe/filter';

@NgModule({
  declarations: [
    MusicListComponent,
    MediaPlayerComponent,
    AudioComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PdfViewerModule,
    HttpClientModule
  ],
  providers: [MusicdataService],
  bootstrap: [MediaPlayerComponent]
})
export class AppModule { }
