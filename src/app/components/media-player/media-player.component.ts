import { Component, OnInit } from '@angular/core';
import { MusicdataService } from 'src/app/services/musicdata.service';
@Component({
  selector: 'media-player',
  templateUrl: './media-player.component.html',
  styleUrls: ['./media-player.component.css']
})
export class MediaPlayerComponent implements OnInit {

  audioList:Array<any>
  selectedItem :any
  zoom: any=1
  groups:Array<any>=[]
  isMultiLang:boolean=false
  selectedLanguage:any
  currentPdf:any
  languages:any=[]
  x={audio:[]}
  constructor(data:MusicdataService) {
    data.getData().subscribe(data=>{
      this.audioList = data.audio
     let tmplist= Array.from(new Set(this.audioList.map((e)=>e.group)));
     this.groups = tmplist.map((e)=>{return {"group":e ,"expanded":false} })
    })
   }

   
  ngOnInit(): void {
    
  }
  addElement(element){
      this.groups.push({"group": element,"expanded":false});
  }
  onSelect(data){
   this.selectedItem = data
   if (typeof(this.selectedItem.pdf) == "object"){
      this.isMultiLang =true
      this.selectedLanguage=(this.selectedItem.pdf["Devanagiri"])? "Devanagiri" : (Object.keys(this.selectedItem.pdf))[0]
      this.currentPdf =  this.selectedItem.pdf[this.selectedLanguage]
      this.languages = Object.keys(this.selectedItem.pdf)
    } else{
      this.isMultiLang =false
      this.currentPdf =  this.selectedItem.pdf
    } 
  }


  zoomIn(){
    this.zoom = this.zoom + 0.1;
  }

  zoomOut(){
    this.zoom = this.zoom - 0.1;
  }
  selectLanguage(language){
    this.selectedLanguage = language
    this.currentPdf = this.selectedItem.pdf[language]
  }

  


}
