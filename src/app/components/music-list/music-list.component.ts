import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'music-list',
  templateUrl: './music-list.component.html',
  styleUrls: ['./music-list.component.css']
})
export class MusicListComponent implements OnInit {

  @Input()
  list:Array<any>
  selectedItem:any
  groupSelected:any
  @Input()
  groups:Array<any>;
  @Output()
  selected:  EventEmitter<any> =   new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    console.log(this.groups)
  }

  onSelect(item){
   this.selected.emit(item)
   this.selectedItem=item

  }
  onSelectGroup(item){
    let selectedGroup =this.groups.find(e=>e.group==item)
    selectedGroup.expanded = !selectedGroup.expanded
  }

}
