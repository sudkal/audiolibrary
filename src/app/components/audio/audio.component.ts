import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import {PdfJsViewerComponent} from 'ng2-pdfjs-viewer'

@Component({
  selector: 'audio-component',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.css']
})
export class AudioComponent implements OnInit {

  private _selectedSong:any

  @ViewChild('player',{static: true}) player: ElementRef;

  constructor() {

  }

  ngOnInit(): void {

  }

  @Input()
  set selectedSong(selected){

    if(selected)
    {
    this._selectedSong = selected
    this.player.nativeElement.load();
    this.player.nativeElement.play();
    }
  }

  get selectedSong(){
    return this._selectedSong

  }


}
