import { getLocaleDateFormat } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class MusicdataService {

  constructor(private http: HttpClient) { 
    
  }

  getData(){
    return this.http.get<any>("assets/musicdata.json")
  }
}
