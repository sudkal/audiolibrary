import { TestBed } from '@angular/core/testing';

import { MusicdataService } from './musicdata.service';

describe('MusicdataService', () => {
  let service: MusicdataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MusicdataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
